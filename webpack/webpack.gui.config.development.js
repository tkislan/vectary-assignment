/* eslint max-len: 0 */
import webpack from 'webpack';
import baseConfig from './webpack.gui.config.base';

const config = {
  ...baseConfig,

  debug: true,

  devtool: 'eval-source-map',

  entry: [
    'webpack-hot-middleware/client?path=http://localhost:4000/__webpack_hmr',
    './src/gui/index',
  ],

  output: {
    ...baseConfig.output,
    path: '/',
    publicPath: '/',
  },

  module: {
    ...baseConfig.module,
    loaders: [
      ...baseConfig.module.loaders,

      {
        test: /\.global\.css$/,
        loaders: [
          'style-loader',
          'css-loader?sourceMap',
        ],
      },

      {
        test: /^((?!\.global).)*\.css$/,
        loaders: [
          'style-loader',
          'css-loader?modules&sourceMap&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
        ],
      },
      {
        test: /\.scss$/,
        loaders: ["style-loader", "css-loader", "sass-loader"]
      },
    ],
  },

  plugins: [
    ...baseConfig.plugins,
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      __DEV__: true,
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      },
    }),
  ],
};

export default config;
