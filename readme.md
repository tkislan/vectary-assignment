# Vectary assignment
## Pantry application

Installation:
```
npm install
```

Database initialization:
```
DATABASE_URL=postgres://<user>:<password>@<hostname>/<database> ./sql/init_database.sh
```

Run application:
```
DATABASE_URL=postgres://<user>:<password>@<hostname>/<database> npm start
```