create table snacks (
  name text
);

create table users (
  name text
);

create table statistics (
  action text,
  user_name text,
  snack_name text,
  occured timestamp default (now())
);
