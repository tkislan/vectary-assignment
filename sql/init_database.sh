#!/bin/bash -xe

if [ -z "$DATABASE_URL" ]; then
  echo "DATABASE_URL environment variable required"
  exit 1
fi

if ! which psql; then
  echo "psql not found"
  exit 1
fi

cd "$(dirname "$0")"

execute_file() {
  echo "PSql: $1"
  psql "$DATABASE_URL" < "$1"
}

execute_file database.sql
