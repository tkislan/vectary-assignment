import React from 'react';

import { Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';

export default class extends React./*Pure*/Component {
  render() {
    const { statistics } = this.props;

    return (
      <div>
        <Table
          style={{ tableLayout: 'auto' }}
          selectable={false}
          fixedHeader={false}
        >
          <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
          >
            <TableRow>
              <TableHeaderColumn>Action</TableHeaderColumn>
              <TableHeaderColumn>User</TableHeaderColumn>
              <TableHeaderColumn>Snack</TableHeaderColumn>
              <TableHeaderColumn>Occured</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
          >
            {statistics.map((s, i) => (
              <TableRow key={i}>
                <TableRowColumn>
                  <span>{s.action}</span>
                </TableRowColumn>
                <TableRowColumn>
                  <span>{s.userName}</span>
                </TableRowColumn>
                <TableRowColumn>
                  <span>{s.snackName}</span>
                </TableRowColumn>
                <TableRowColumn>
                  <span>{s.occured}</span>
                </TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    );
  }
}
