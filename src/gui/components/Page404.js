import React from 'react';

import './Page404.scss';

export default class Page404 extends React./*Pure*/Component {
  render() {
    return (
      <div className="page-404">
        <h1>Page not found</h1>
      </div>
    );
  }
}
