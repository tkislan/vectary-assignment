import React from 'react';

import MainContent from './MainContent';
import Notifications from './Notifications';

import './App.scss';

export default class App extends React./*Pure*/Component {
  render() {
    const { router, params, notification, snacks, users, statistics, snacksAdd, snacksTake, usersAdd } = this.props;

    return (
      <div className="app-container">
        <div><h1>PantryApp</h1></div>
        <MainContent
          tab={params.tab}
          routerPush={router.push}
          snacks={snacks}
          users={users}
          statistics={statistics}
          onSnacksAdd={snacksAdd}
          onSnacksTake={snacksTake}
          onUsersAdd={usersAdd}
        />
        <Notifications
          notification={notification}
        />
      </div>
    );
  }
}
