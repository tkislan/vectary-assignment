import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import App from './App';

import { snacksAdd, usersAdd, snacksTake } from '../store/actions';

function mapStateToProps(state) {
  return {
    snacks: state.app.snacks,
    users: state.app.users,
    statistics: state.app.statistics,
    notification: state.app.notification,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ snacksAdd, usersAdd, snacksTake }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
