import React from 'react';

import Paper from 'material-ui/Paper';
import { Tabs, Tab } from 'material-ui/Tabs';

import AddDialog from './AddDialog';
import SnackList from './SnackList';
import Statistics from './Statistics';

import './MainContent.scss';

export default class MainContent extends React./*Pure*/Component {
  handleTabChange = (value) => {
    this.props.routerPush(value === 'list' ? '/' : value);
  };

  handleAddUser = (userName) => {
    console.log('Add user:', userName);
    this.props.onUsersAdd(userName);
  };

  handleAddSnack = (snackName) => {
    console.log('Add snack:', snackName);
    this.props.onSnacksAdd(snackName);
  };

  render() {
    const { snacks, users, statistics, onSnacksTake } = this.props;

    return (
      <Paper className="main-content">
        <Tabs
          className="tabs"
          contentContainerClassName="tab-container"
          tabTemplateStyle={{ height: '100%', display: 'flex', flexDirection: 'column' }}
          value={this.props.tab || 'list'}
          onChange={this.handleTabChange}
        >
          <Tab label="List" value="list">
            <div className="add-buttons">
              <AddDialog
                title="Add user"
                onSubmit={this.handleAddUser}
              />
              <AddDialog
                title="Add snack"
                onSubmit={this.handleAddSnack}
              />
            </div>
            <SnackList
              snacks={snacks}
              users={users}
              onSnacksTake={onSnacksTake}
            />
          </Tab>
          <Tab label="Statistics" value="stats">
            <Statistics
              statistics={statistics}
            />
          </Tab>
        </Tabs>
      </Paper>
    );
  }
}
