import React from 'react';

import Snackbar from 'material-ui/Snackbar';

export default class Notifications extends React./*Pure*/Component {
  state = {
    open: false,
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.notification !== nextProps.notification) this.setState({ open: true });
    // this.setState({ open: true });
  }

  handleRequestClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { notification } = this.props;

    return (
      <Snackbar
        open={this.state.open}
        message={notification.message || ''}
        autoHideDuration={3000}
        onRequestClose={this.handleRequestClose}
      />
    );
  }
}
