import React from 'react';

import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

export default class AddDialog extends React./*Pure*/Component {
  state = {
    open: false,
    name: '',
  };

  handleSetName = (e) => { this.setState({ name: e.target.value }); };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false, name: '' });
  };

  handleSubmit = () => {
    this.props.onSubmit(this.state.name);
    this.handleClose();
  };

  render() {
    const { title } = this.props;
    const { open, name } = this.state;

    const disabledSubmitButton = this.state.name.length === 0;

    const actions = [
      <RaisedButton
        label="Cancel"
        secondary
        onTouchTap={this.handleClose}
      />,
      <RaisedButton
        label="Add"
        primary
        keyboardFocused
        onTouchTap={this.handleSubmit}
        disabled={disabledSubmitButton}
      />,
    ];

    return (
      <div className="add-dialog">
        <RaisedButton
          label={title}
          secondary
          onTouchTap={this.handleOpen}
        />
        <Dialog
          contentStyle={{ width: '500px' }}
          title={title}
          actions={actions}
          modal={false}
          open={open}
          onRequestClose={this.handleClose}
        >
          <TextField
            floatingLabelText="Name"
            value={name}
            onChange={this.handleSetName}
          />
        </Dialog>
      </div>
    );
  }
}
