import React from 'react';

import Paper from 'material-ui/Paper';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';

import './SnackList.scss';

export default class SnackList extends React./*Pure*/Component {
  state = {
    selectedSnack: localStorage.selectedSnack || null,
    selectedUser: localStorage.selectedUser || null,
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.snacks !== nextProps.snacks) {
      if (!nextProps.snacks.filter((s) => s.name === this.state.selectedSnack)[0]) {
        this.setState({ selectedSnack: null });
      }
    }
  }

  handleSelectSnack = (e, selectedSnack) => {
    this.setState({ selectedSnack });
    localStorage.setItem("selectedSnack", selectedSnack);
  };

  handleSelectUser = (e, index, selectedUser) => {
    this.setState({ selectedUser });
    localStorage.setItem("selectedUser", selectedUser);
    localStorage.setItem("selectedSnack", null);
  };

  handleTakeSnack = () => {
    this.props.onSnacksTake(this.state.selectedSnack, this.state.selectedUser);
  };

  render() {
    const { selectedSnack, selectedUser } = this.state;
    const { snacks, users } = this.props;

    return (
      <Paper className="snack-list">
        {snacks.length === 0 && (
          <div>No snacks in pantry</div>
        )}
        {snacks.length > 0 && (
          <RadioButtonGroup
            name="snack"
            valueSelected={selectedSnack}
            onChange={this.handleSelectSnack}
          >
            {snacks.map((s) => (
              <RadioButton
                key={s.name}
                value={s.name}
                label={`${s.name}: ${s.count}`}
              />
            ))}
          </RadioButtonGroup>
        )}

        <SelectField
          floatingLabelText={users.length > 0 ? 'User' : 'No users'}
          value={selectedUser}
          onChange={this.handleSelectUser}
          disabled={users.length === 0}
        >
          {users.map((u) => (
            <MenuItem key={u} value={u} primaryText={u} />
          ))}
        </SelectField>

        <RaisedButton
          label="Take snack"
          onTouchTap={this.handleTakeSnack}
          disabled={!selectedSnack || !selectedUser}
        />
      </Paper>
    );
  }
}
