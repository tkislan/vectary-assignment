import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

import App from './components/App_Redux';

import Page404 from './components/Page404';


export default function (store) {
  return (
    <Route path="/(:tab)" component={App} />
  );
}
