import axios from 'axios';

export const SNACKS_GET_ALL = 'SNACKS_GET_ALL';
export const SNACKS_ADD = 'SNACKS_ADD';
export const SNACKS_TAKE = 'SNACKS_TAKE';
export const USERS_GET_ALL = 'USERS_GET_ALL';
export const USERS_ADD = 'USERS_ADD';
export const STATISTICS_GET_ALL = 'STATISTICS_GET_ALL';

export function snacksGetAll() {
  return {
    type: SNACKS_GET_ALL,
    payload: axios.get('/api/snacks'),
  };
}

export function snacksAdd(snackName) {
  return {
    type: SNACKS_ADD,
    meta: {
      snackName,
    },
    payload: axios.post('/api/snacks/add', { snackName }),
  };
}

export function snacksTake(snackName, userName) {
  return {
    type: SNACKS_TAKE,
    meta: {
      snackName,
      userName,
    },
    payload: axios.post('/api/snacks/take', { snackName, userName }),
  };
}

export function usersGetAll() {
  return {
    type: USERS_GET_ALL,
    payload: axios.get('/api/users'),
  };
}

export function usersAdd(userName) {
  return {
    type: USERS_ADD,
    meta: {
      userName,
    },
    payload: axios.post('/api/users/add', { userName }),
  };
}

export function statisticsGetAll() {
  return {
    type: STATISTICS_GET_ALL,
    payload: axios.get('/api/statistics'),
  };
}
