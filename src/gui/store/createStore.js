import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { browserHistory, hashHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';

import rootReducer from './reducer';

export default (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const commonMiddleware = [thunk, promiseMiddleware(), routerMiddleware(hashHistory)];

  let middleware = [...commonMiddleware];

  if (__DEV__) {
    const createLogger = require('redux-logger');
    middleware = [...middleware, createLogger()];
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middleware),
      // ...enhancers
    )
  );

  // To unsubscribe, invoke `store.unsubscribeHistory()` anytime
  // store.unsubscribeHistory = browserHistory.listen(updateLocation(store))

  if (module.hot) {
    module.hot.accept('./reducer', () => {
      const reducers = require('./reducer').default
      store.replaceReducer(reducers(store.asyncReducers))
    });
  }

  console.log('Store created');

  return store;
}
