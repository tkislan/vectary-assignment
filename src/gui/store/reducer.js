import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import { SNACKS_GET_ALL, SNACKS_ADD, SNACKS_TAKE, USERS_GET_ALL, USERS_ADD, STATISTICS_GET_ALL } from './actions';

const initialState = {
  pendingSnacksGetAll: false,
  pendingSnacksAdd: false,
  pendingSnacksTake: false,
  pendingUsersGetAll: false,
  pendingUsersAdd: false,
  pendingStatisticsGetAll: false,
  snacks: [],
  users: [],
  statistics: [],
  notification: { message: null },
};

function addSnack(snacks, snackName) {
  if (snacks.filter((s) => s.name === snackName)[0]) {
    return snacks.map((s) => s.name === snackName ? { ...s, count: s.count + 1 } : s);
  } else {
    return [...snacks, { name: snackName, count: 1 }];
  }
}

function takeSnack(snacks, snackName) {
  return snacks
    .map((s) => s.name === snackName ? { ...s, count: s.count - 1 } : s)
    .filter((s) => s.count > 0);
}

function getOccured() {
  const tmp = (new Date()).toString();
  return tmp.substr(0, tmp.indexOf('GMT') - 1);
}

const app = (state = initialState, action) => {
  switch (action.type) {
    case `${SNACKS_GET_ALL}_PENDING`:
      return { ...state, pendingSnacksGetAll: true };
    case `${SNACKS_GET_ALL}_FULFILLED`:
      return { ...state, pendingSnacksGetAll: false, snacks: action.payload.data };
    case `${SNACKS_GET_ALL}_REJECTED`:
      return { ...state, pendingSnacksGetAll: false };

    case `${SNACKS_ADD}_PENDING`:
      return { ...state, pendingSnacksAdd: true };
    case `${SNACKS_ADD}_FULFILLED`:
      return {
        ...state,
        pendingSnacksAdd: false,
        snacks: addSnack(state.snacks, action.meta.snackName),
        notification: { message: `Added snack: ${action.meta.snackName}` },
        statistics: [...state.statistics, {
          action: 'ADD',
          userName: null,
          snackName: action.meta.snackName,
          occured: getOccured(),
        }],
      };
    case `${SNACKS_ADD}_REJECTED`:
      return { ...state, pendingSnacksAdd: false };

    case `${SNACKS_TAKE}_PENDING`:
      return { ...state, pendingSnacksTake: true };
    case `${SNACKS_TAKE}_FULFILLED`:
      return {
        ...state,
        pendingSnacksTake: false,
        snacks: takeSnack(state.snacks, action.meta.snackName),
        notification: { message: `${action.meta.userName} has taken ${action.meta.snackName} out of pantry` },
        statistics: [...state.statistics, {
          action: 'TAKE',
          userName: action.meta.userName,
          snackName: action.meta.snackName,
          occured: getOccured(),
        }],
      };
    case `${SNACKS_TAKE}_REJECTED`:
      return { ...state, pendingSnacksTake: false };

    case `${USERS_GET_ALL}_PENDING`:
      return { ...state, pendingUsersGetAll: true };
    case `${USERS_GET_ALL}_FULFILLED`:
      return { ...state, pendingUsersGetAll: false, users: action.payload.data.map((u) => u.name) };
    case `${USERS_GET_ALL}_REJECTED`:
      return { ...state, pendingUsersGetAll: false };

    case `${USERS_ADD}_PENDING`:
      return { ...state, pendingUsersAdd: true };
    case `${USERS_ADD}_FULFILLED`:
      return {
        ...state,
        pendingUsersAdd: false,
        users: [...state.users, action.meta.userName],
        notification: { message: `Added user ${action.meta.userName}` },
      };
    case `${USERS_ADD}_REJECTED`:
      return { ...state, pendingUsersAdd: false };

    case `${STATISTICS_GET_ALL}_PENDING`:
      return { ...state, pendingStatisticsGetAll: true };
    case `${STATISTICS_GET_ALL}_FULFILLED`:
      return { ...state, pendingStatisticsGetAll: false, statistics: action.payload.data };
    case `${STATISTICS_GET_ALL}_REJECTED`:
      return { ...state, pendingStatisticsGetAll: false };

    default:
      return state;
  }
}

export default combineReducers({
  app,
  routing,
});
