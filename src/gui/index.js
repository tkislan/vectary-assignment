import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, Route, browserHistory, hashHistory } from 'react-router';

import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import createStore from './store/createStore';

import routes from './routes';

import { snacksGetAll, usersGetAll, statisticsGetAll } from './store/actions';

injectTapEventPlugin();

// ========================================================
// Store Instantiation
// ========================================================
const initialState = { ...window.___INITIAL_STATE___ };
const store = createStore(initialState);

store.dispatch(snacksGetAll());
store.dispatch(usersGetAll());
store.dispatch(statisticsGetAll());

const history = syncHistoryWithStore(hashHistory, store);

setTimeout(() => {

ReactDOM.render(
  <MuiThemeProvider>
    <Provider store={store}>
      <Router history={history}>
        {routes(store)}
      </Router>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root')
);

}, 2000);
