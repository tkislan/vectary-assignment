/* eslint no-console: 0 */

import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

import config from '../../webpack/webpack.gui.config.development';

import setupServer from './setup-server';

let wdm;

setupServer({ customAppHook: (app) => {
  const compiler = webpack(config);

  wdm = webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
    stats: {
      colors: true,
      chunks: false,
    },
  });

  app.use(wdm);

  app.use(webpackHotMiddleware(compiler));
}, customSigtermHook: () => {
  if (wdm) wdm.close();
}});
