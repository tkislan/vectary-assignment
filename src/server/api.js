import { Router } from 'express';

export default function({ db }) {
  const router = Router({ mergeParams: true });

  router.get('/snacks', (req, res) => {
    db.getSnacks((error, result) => {
      if (error) return res.status(400).json({ error: error.message });

      return res.json(result.rows);
    });
  });

  router.post('/snacks/add', (req, res) => {
    const { snackName } = req.body;

    db.addSnack(snackName, (error) => {
      if (error) return res.status(400).json({ error: error.message });

      db.addStatistic('ADD', null, snackName, (error) => {
        if (error) return console.error('Failed to add statistic:', error);
      });

      return res.status(200).end();
    });
  });

  router.post('/snacks/take', (req, res) => {
    const { userName, snackName } = req.body;

    db.takeSnack(userName, snackName, (error, result) => {
      if (error) return res.status(400).json({ error: error.message });
      if (result.rowCount === 0) return res.status(400).json({ error: 'Snack doesn\'t exist' });

      db.addStatistic('TAKE', userName, snackName, (error) => {
        if (error) return console.error('Failed to add statistic:', error);
      });

      return res.status(200).end();
    });
  });

  router.get('/users', (req, res) => {
    db.getUsers((error, result) => {
      if (error) return res.status(400).json({ error: error.message });

      return res.json(result.rows);
    });
  });

  router.post('/users/add', (req, res) => {
    const { userName } = req.body;

    db.addUser(userName, (error, result) => {
      if (error) return res.status(400).json({ error: error.message });

      return res.status(200).end();
    })
  });

  router.get('/statistics', (req, res) => {
    db.getStatistics((error, result) => {
      if (error) return res.status(400).json({ error: error.message });

      return res.json(result.rows.map((r) => ({ action: r.action, userName: r.user_name, snackName: r.snack_name, occured: r.occured })));
    });
  });

  return router;
}
