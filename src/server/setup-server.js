import path from 'path';
import { Server } from 'http';
import express from 'express';
import bodyParser from 'body-parser';

import * as db from './database';
import apiRouter from './api';

export default function setupServer({
  customAppHook = () => {},
  customSigtermHook = () => {},
}) {
  const app = express();
  const server = Server(app);

  app.use(bodyParser.json()); // for parsing application/json
  app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

  customAppHook(app);

  app.use('/api', apiRouter({ db }));

  const PORT = process.env.PORT || 4000;

  server.listen(PORT, (error) => {
    if (error) {
      console.error(error);
      return;
    }

    console.log(`Listening at http://*:${PORT}`);
  });

  process.on('SIGTERM', () => {
    console.log('Stopping server');
    customSigtermHook();
    server.close(() => {
      process.exit(0);
    });
  });
}
