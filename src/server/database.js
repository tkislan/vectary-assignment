import pg from 'pg';

if (!process.env.DATABASE_URL) throw new Error('DATABASE_URL environment variable not defined');

const connString = process.env.DATABASE_URL;

function pgGetClient(cb) {
  pg.connect(connString, (error, client, done) => {
    if (error) return cb(error);

    return cb(null, { client, done });
  });
}

function pgRunQuery(query, params, cb) {
  pgGetClient((error, { client, done }) => {
    if (error) return cb(error);

    client.query(query, params, (error, result) => {
      done();
      return cb(error, result);
    });
  });
}

export function getSnacks(cb) {
  pgRunQuery('SELECT name, count(name) as count FROM snacks GROUP BY name', [], cb);
};

export function addSnack(snackName, cb) {
  pgRunQuery('INSERT INTO snacks VALUES ($1)', [snackName], cb);
};

export function takeSnack(userName, snackName, cb) {
  pgRunQuery('DELETE FROM snacks WHERE ctid IN (SELECT ctid FROM snacks WHERE name = $1 LIMIT 1)', [snackName], cb);
}

export function getUsers(cb) {
  pgRunQuery('SELECT name FROM users', [], cb);
}

export function addUser(userName, cb) {
  pgRunQuery('INSERT INTO users VALUES ($1)', [userName], cb);
}

export function addStatistic(action, userName, snackName, cb) {
  pgRunQuery(
    'INSERT INTO statistics (action, user_name, snack_name) VALUES ($1, $2, $3)',
    [action, userName, snackName],
    cb
  );
}

export function getStatistics(cb) {
  pgRunQuery('SELECT action, user_name, snack_name, to_char(occured, \'Dy, DD Mon YYYY HH24:MI:SS\') as occured FROM statistics ORDER BY occured ASC', [], cb);
}
