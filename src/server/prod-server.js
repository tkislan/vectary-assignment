// import fallback from 'express-history-api-fallback';

/* eslint no-console: 0 */

import express from 'express';

import setupServer from './setup-server';


setupServer({ customAppHook: (app) => {
  console.log(`Prod server dirname: ${__dirname}`);

  app.use(express.static('dist/src/gui'));
}});
